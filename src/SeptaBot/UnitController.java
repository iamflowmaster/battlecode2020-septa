package SeptaBot;

import battlecode.common.GameActionException;

public abstract class UnitController extends Controller {
    UnitController() throws GameActionException {
        super();
        Terrain.init();
    }

    public void run() throws GameActionException {
        // обновляем ландшафт
        Sensor.refreshTerrain(true);

        // читаем непрочитанные транзакции
        Sensor.readTransactions();
        // каждый раунд обновляем роботов
        Sensor.refreshRobots();
    }
}
