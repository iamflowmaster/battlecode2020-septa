package SeptaBot;

import battlecode.common.*;
import java.util.*;

public class DesignSchoolController extends BuildingController {
    private int landscapersCreated = 0;
    private Queue<MapLocation> groundingPositions;
    private int[] groundingOrder = {-1, 1, -3, 3, 0, -2, 2, -4};

    DesignSchoolController() throws GameActionException{
        super();
    }

    public void postInit() throws GameActionException {
        super.postInit();

        if (globalTask == GlobalTask.PRIMARY_BUILDING)
            initGroundingPositions();
    }

    public void run() throws GameActionException {
        Sensor.readTransactions();

        switch (globalTask) {
            case GROUNDING:
                System.out.println("GROUNDING");
                for (Direction dir : Direction.allDirections()) {
                    if (BuildUtils.tryBuild(RobotType.LANDSCAPER, dir)) {
                        RobotInfo landscaper = RobotPlayer.rc.senseRobotAtLocation(
                                RobotPlayer.rc.adjacentLocation(dir));

                        // первые лэндскейперы направляются к HQ, чтобы заземляться
                        TransactionUtils.addTransactionToTheQueue(
                                TransactionUtils.callGrounderMessage(landscaper.ID, groundingPositions.poll()), 1);

                        landscapersCreated++;

                        // заканчиваем стадию заземления, переходим к терраформированию
                        if (groundingPositions.isEmpty()) {
                            TransactionUtils.addTransactionToTheQueue(
                                    TransactionUtils.globalTaskMessage(GlobalTask.TERRAFORMING), 5);
                        }

                        break;
                    }
                }
                break;

            case TERRAFORMING:
                break;
        }

        TransactionUtils.tryToSendFromQueue();
    }

    public void initGroundingPositions() {
        groundingPositions = new LinkedList<>();

        MapLocation hqLoc = ObjectsCache.getFirstBuilding(RobotType.HQ, RobotPlayer.team).location;
        int oppositeHQDirOrdinal = RobotPlayer.rc.getLocation().directionTo(hqLoc).ordinal();

        for (int dirIndex : groundingOrder) {
            dirIndex = MoveUtils.normalizeDirectionOrdinal(oppositeHQDirOrdinal + dirIndex, 8);

            Direction dir = RobotPlayer.directions[dirIndex];
            MapLocation groundingLoc = hqLoc.translate(dir.dx, dir.dy);
            if (Terrain.onTheMap(groundingLoc)) {
                groundingPositions.add(groundingLoc);
            }
        }
        System.out.println(groundingPositions);
    }
}
