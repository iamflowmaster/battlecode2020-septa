package SeptaBot;
import battlecode.common.*;

public class RobotPlayer {
    static RobotController rc;
    static Controller c;    // объект контроллера определенного типа робота
    static RobotType robotType;
    static Team team;

    static Direction[] directions = {Direction.NORTH, Direction.NORTHEAST, Direction.EAST, Direction.SOUTHEAST,
            Direction.SOUTH, Direction.SOUTHWEST, Direction.WEST,Direction.NORTHWEST};

    @SuppressWarnings("unused")
    public static void run(RobotController rc) throws GameActionException {
        // This is the RobotController object. You use it to perform actions from this robot,
        // and to get information on its current status.
        RobotPlayer.rc = rc;

        init();

        System.out.println("I'm a " + rc.getType() + " and I just got created!");
        while (true) {
            try {
                c.run();
                Clock.yield();
            } catch (Exception e) {
                System.out.println(rc.getType() + " Exception");
                e.printStackTrace();
            }
        }
    }

    public static void init() throws GameActionException {
        robotType = RobotPlayer.rc.getType();
        team = RobotPlayer.rc.getTeam();

        switch (robotType) {
            case HQ: RobotPlayer.c = new HQController(); break;
            case DELIVERY_DRONE: RobotPlayer.c = new DeliveryDroneController(); break;
            case DESIGN_SCHOOL: RobotPlayer.c = new DesignSchoolController(); break;
            case FULFILLMENT_CENTER: RobotPlayer.c = new FulfillmentCenterController(); break;
            case LANDSCAPER: RobotPlayer.c = new LandscaperController(); break;
            case REFINERY: RobotPlayer.c = new RefineryController(); break;
            case NET_GUN: RobotPlayer.c = new NetGunController(); break;
            case VAPORATOR: RobotPlayer.c = new VaporatorController();
            case MINER: RobotPlayer.c = new MinerController();
        }

        // при инициализации каждый робот читает весь блокчейн
        c.postInit();
    }
}

