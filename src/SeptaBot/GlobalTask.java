package SeptaBot;

public enum GlobalTask {
    EARLY_MINERS,
    PRIMARY_BUILDING,
    GROUNDING,
    TERRAFORMING
}
