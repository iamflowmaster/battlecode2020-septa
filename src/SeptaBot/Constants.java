package SeptaBot;

public class Constants {
    public final static int EXPLORING_THRESHOLD = Terrain.map_width * 5;

    public final static int TRANSACTION_MESSAGE_CODE = 117901063;

    public enum MessageType {
        SOUP_LOCATION,
        BUILDING_INFO,
        GLOBAL_TASK,
        CALL_EXPEDITOR,
        CALL_BUILDER,
        CALL_GROUNDER
    }

    public final static int TERRAIN_REFRESH_COOLDOWN = 2;

    public final static int REFINERY_MIN_DISTANCE = 35;
    public final static int EARLY_MINERS_NUMBER = 5;
    public final static int MINER_INVENTORY_MAXIMUM = 70;

    public final static int OBSTACLE_MOVEMENT_THRESHOLD = 20;
}
