package SeptaBot;

import battlecode.common.*;

public class DeliveryDroneController extends UnitController {
    public DeliveryDroneController() throws GameActionException {
        super();
    }

    public void run() throws GameActionException {
        super.run();

        // Обязательно проверяем возможность совершения действия, т.к
        // в дальнейшем коде ожидается, что действие возможно совершить.
        if (!RobotPlayer.rc.isReady())
            return;

    }
}
