package SeptaBot;

import battlecode.common.*;

import java.util.*;

/**
 * Сообщение транзакции состоит из 7-ми целых чисел.
 * Целые числа по индексам означают:
 * 0. Код сообщения транзакции. Пометка транзакции, как транзакции, принадлежащей нашей команде.
 * 1. Тип сообщения
 * 2 - 6. Сообщение
 *
 * Типы сообщений:
 */
public class TransactionUtils {
    public static int startReadFromRound = 1;
    public static Queue<TransactionQueueItem> transactionsQueue = new LinkedList<>();

    public static void addTransactionToTheQueue(int[] message, int cost) {
        transactionsQueue.add(new TransactionQueueItem(message, cost));
    }

    public static void tryToSendFromQueue() throws GameActionException {
        while (true) {
            TransactionQueueItem transaction = transactionsQueue.peek();
            if (transaction == null || !RobotPlayer.rc.canSubmitTransaction(transaction.message, transaction.cost))
                break;

            RobotPlayer.rc.submitTransaction(transaction.message, transaction.cost);
            transactionsQueue.remove();
        }
    }

    public static void decodeTransactionBlock(Transaction[] block) {
        for (Transaction transaction : block) {
            int[] message = transaction.getMessage();

            int messageCode = message[0];
            // TODO не нужно читать собственные транзакции
            if (messageCode == Constants.TRANSACTION_MESSAGE_CODE + ((RobotPlayer.team == Team.B) ? 1:0)) {
                Constants.MessageType messageType = Constants.MessageType.values()[message[1]];
                switch (messageType) {
                    case CALL_EXPEDITOR:
                        serializeCallExpeditor(message);
                        break;
                    case CALL_BUILDER:
                        serializeCallBuilder(message);
                        break;
                    case SOUP_LOCATION:
                        serializeSoupLocation(message);
                        break;
                    case BUILDING_INFO:
                        serializeBuildingInfo(message);
                        break;
                    case GLOBAL_TASK:
                        serializeGlobalTask(message);
                        break;
                    case CALL_GROUNDER:
                        serializeCallGrounder(message);
                }
            }
        }
    }

    public static int[] encodeMessage(Constants.MessageType type, int[] message) {
        int[] encodedMessage = new int[7];

        encodedMessage[0] = Constants.TRANSACTION_MESSAGE_CODE + ((RobotPlayer.team == Team.B) ? 1:0);
        encodedMessage[1] = type.ordinal();

        System.arraycopy(message, 0, encodedMessage, 2, message.length);
        return encodedMessage;
    }

    /**
     * Сериализация супа. Индексы сообщения:
     * 2. Координата X.
     * 3. Координата Y.
     * @param message
     */
    public static void serializeSoupLocation(int[] message) {
        int x = message[2];
        int y = message[3];
        MapLocation soupLoc = new MapLocation(x, y);

        ObjectsCache.soup.add(soupLoc);
    }

    public static int[] soupLocationMessage(MapLocation soupLoc) {
        return encodeMessage(Constants.MessageType.SOUP_LOCATION, new int[] {soupLoc.x, soupLoc.y});
    }


    /**
     * Сериализация супа. Индексы сообщения:
     * 2. ID строения
     * 3. Тип строения
     * 4. Команда
     * 5. Координата X.
     * 6. Координата Y.
     * @param message
     */
    public static void serializeBuildingInfo(int[] message) {
        RobotType robotType = RobotType.values()[message[3]];
        Team team = Team.values()[message[4]];
        MapLocation loc = new MapLocation(message[5], message[6]);

        RobotInfo buildingInfo = new RobotInfo(message[2], team, robotType, 0, false, 0, 0, 0.0f, loc);

        ObjectsCache.buildings.add(buildingInfo);
    }

    public static int[] buildingInfoMessage(RobotInfo buildingInfo) {
        return encodeMessage(Constants.MessageType.BUILDING_INFO, new int[] {
                buildingInfo.ID, buildingInfo.type.ordinal(), buildingInfo.team.ordinal(),
                buildingInfo.location.x, buildingInfo.location.y});
    }

    /**
     * 2. Глобальная задача.
     * @param message
     */
    public static void serializeGlobalTask(int[] message) {
        RobotPlayer.c.globalTask = GlobalTask.values()[message[2]];
    }

    public static int[] globalTaskMessage(GlobalTask globalTask) {
        return encodeMessage(Constants.MessageType.GLOBAL_TASK, new int[] {globalTask.ordinal()});
    }

    /**
     * 2. ID.
     * @param message
     */
    public static void serializeCallExpeditor(int[] message) {
        if (RobotPlayer.robotType == RobotType.MINER && RobotPlayer.rc.getID() == message[2])
            ((MinerController) RobotPlayer.c).initExpedition();
    }

    public static int[] callExpeditorMessage(int id) {
        return encodeMessage(Constants.MessageType.CALL_EXPEDITOR, new int[] {id});
    }


    /**
     * 2. ID.
     * @param message
     */
    public static void serializeCallBuilder(int[] message) {
        if (RobotPlayer.robotType == RobotType.MINER && RobotPlayer.rc.getID() == message[2])
            ((MinerController) RobotPlayer.c).isPrimaryBuilder = true;
    }

    public static int[] callBuilderMessage(int id) {
        return encodeMessage(Constants.MessageType.CALL_BUILDER, new int[] {id});
    }

    /**
     * 2. ID.
     * 3. X заземления.
     * 4. Y заземления.
     * @param message
     */
    public static void serializeCallGrounder(int[] message) {
        if (RobotPlayer.robotType == RobotType.LANDSCAPER && RobotPlayer.rc.getID() == message[2]) {
            LandscaperController c = ((LandscaperController) RobotPlayer.c);
            c.initGrounding(new MapLocation(message[3], message[4]));
        }
    }

    public static int[] callGrounderMessage(int id, MapLocation groundingLoc) {
        return encodeMessage(Constants.MessageType.CALL_GROUNDER, new int[] {id, groundingLoc.x, groundingLoc.y});
    }
}


class TransactionQueueItem {
    public int[] message;
    public int cost;

    TransactionQueueItem(int[] message, int cost) {
        this.message = message;
        this.cost = cost;
    }
}