package SeptaBot;

import battlecode.common.*;

public class CombatUtils {
    public static void tryToShootNearestDrone() throws GameActionException {
        if (!RobotPlayer.rc.isReady())
            return;

        RobotInfo enemyDrone = ObjectsCache.getNearestUnit(RobotPlayer.team.opponent(), RobotType.DELIVERY_DRONE);

        if ((enemyDrone != null) && (RobotPlayer.rc.canShootUnit(enemyDrone.ID))) {
            RobotPlayer.rc.shootUnit(enemyDrone.ID);
            ObjectsCache.units.remove(enemyDrone);
        }
    }
}
