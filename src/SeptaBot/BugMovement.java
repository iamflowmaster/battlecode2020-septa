package SeptaBot;

import battlecode.common.*;
import com.sun.tools.classfile.ConstantPool;

import java.util.*;

public class BugMovement {
    public static MapLocation start;
    public static MapLocation goal;
    public static Direction dirFromStartToGoal;

    private static int minDistanceToTheGoal;

    private static boolean isMovingAroundObstacle = false;
    private static MapLocation curObstacleLoc;
    private static MapLocation obstacleCollisionLoc;
    private static Direction lastMoveDir = Direction.NORTH;
    private static int[] ordinalObstacleOffsetOrder = {0, 1, 2, 3, 4, 5, 6, 7};
    private static int[] ordinalMoveOffsetCardinalOrder = {-1, 1, 0, -2, 2, -3, 3, -4};
    private static int[] ordinalMoveOffsetDiagonalOrder = {0, -1, 1, -2, 2, -3, 3, -4};
    private static int[] minDistanceMoveCheckOrder = {1, 3, 5, 7, 0, 2, 4, 6};
    private static int obstacleMovementCounter;


    public static void setGoal(MapLocation goal) {
        if (BugMovement.goal == goal)
            return;

        BugMovement.start = RobotPlayer.rc.getLocation();
        BugMovement.goal = goal;
        BugMovement.minDistanceToTheGoal = getDistanceToTheGoal(RobotPlayer.rc.getLocation());
        BugMovement.dirFromStartToGoal = BugMovement.start.directionTo(goal);
    }

    public static boolean moveToGoal() throws GameActionException {
        System.out.println("Goal is " + goal);
        MapLocation robotLoc = RobotPlayer.rc.getLocation();

        if (!haveGoal())
            return false;

        // если цель - это здание, то у нас никогда не получится его достигнуть.
        // поэтому проверяем соседство
        if (isReachedGoal()) {
            return true;
        }



        // 1. Пробуем передвижение на ячейку, которая ближе уже минимально достигнутой ячейки
        System.out.println("Trying to go closer " + minDistanceToTheGoal);
        Direction minDir = null;
        int minDistanceDir = minDistanceToTheGoal;

        for (Direction dir : RobotPlayer.directions) {
            MapLocation loc = RobotPlayer.rc.adjacentLocation(dir);
            int distance = getDistanceToTheGoal(loc);

            if (distance < minDistanceDir && Terrain.canStepFromTo(robotLoc, loc)) {
                minDistanceDir = distance;
                minDir = dir;
            }
        }

        if (minDir != null) {
            minDistanceToTheGoal = minDistanceDir;
            isMovingAroundObstacle = false;
            MoveUtils.tryToMove(minDir);
            return true;
        }

        // 2. Если не получилось сходить ближе, чем было раньше, то двигаемся вокруг препятствия
        System.out.println("Moving around the obstacle.");

        if (!isMovingAroundObstacle) {
            Direction goalDir = robotLoc.directionTo(goal);
            isMovingAroundObstacle = true;
            obstacleMovementCounter = 0;
            curObstacleLoc = RobotPlayer.rc.adjacentLocation(goalDir);
            obstacleCollisionLoc = robotLoc;
            lastMoveDir = goalDir;
        }

        return moveAroundObstacle();
    }

    private static int getDistanceToTheGoal(MapLocation loc) {
        return loc.distanceSquaredTo(goal);
    }

    private static boolean moveAroundObstacle() throws GameActionException {
        // обновляем попытку направления к цели, если юнит слишком долго ходит
        if (obstacleMovementCounter == Constants.OBSTACLE_MOVEMENT_THRESHOLD) {
            BugMovement.minDistanceToTheGoal = getDistanceToTheGoal(RobotPlayer.rc.getLocation());
            return true;
        }

        obstacleMovementCounter++;

        MapLocation robotLoc = RobotPlayer.rc.getLocation();
        Direction moveDir = null;

        founded: {
            int lastMoveOrdinal = lastMoveDir.ordinal();

            int[] ordinalMoveOffsetOrder =
                    (lastMoveOrdinal % 2 == 0) ? ordinalMoveOffsetCardinalOrder : ordinalMoveOffsetDiagonalOrder;

            for (int nextMoveOffset : ordinalMoveOffsetOrder) {
                int nextMoveDirOrdinal = MoveUtils.normalizeDirectionOrdinal(
                        lastMoveOrdinal + nextMoveOffset, RobotPlayer.directions.length);

                Direction nextDir = RobotPlayer.directions[nextMoveDirOrdinal];
                System.out.println("Checking direction " + nextDir);

                MapLocation nextLoc = robotLoc.translate(nextDir.dx, nextDir.dy);

                if (RobotPlayer.rc.canMove(nextDir) && !Terrain.getFlooding(nextLoc)) {
                    System.out.println("Can move ");

                    // если слева есть стенка, то это направление движения подходит
                    Direction obstacleDir = nextDir;
                    for (int i = 0; i < 3; i++) {
                        obstacleDir = obstacleDir.rotateLeft();
                        MapLocation obstacleLoc = nextLoc.translate(obstacleDir.dx, obstacleDir.dy);
                        if (!Terrain.canStepFromTo(nextLoc, obstacleLoc)) {
                            System.out.println("ACCEPT");
                            moveDir = nextDir;
                            // если твердый объект - это юнит, тогда обнулим минимальную дистанцию
                            if (ObjectsCache.containsUnitAtLocation(obstacleLoc)) {
                                minDistanceToTheGoal = getDistanceToTheGoal(obstacleLoc);
                            }
                            break founded;
                        }
                    }
                }
            }
        }

        System.out.println(moveDir);

        // не можем продолжать, если что-то мешает идти дальше
        if (!MoveUtils.tryToMove(moveDir)) {
            System.out.println("Can't continue to move around obstacle. Something is bothering.");
            curObstacleLoc = null;
            isMovingAroundObstacle = false;
            return false;
        }

        lastMoveDir = moveDir;

        // если движение совершено, то обновляем позицию
        robotLoc = RobotPlayer.rc.getLocation();

        // не можем продолжать, если вернулись на то место, с которого начинали обходить препятствие
        if (robotLoc.equals(obstacleCollisionLoc)) {
            System.out.println("Walked around obstacle. No way to the goal.");
            isMovingAroundObstacle = false;
            curObstacleLoc = null;
            goal = null;
            return false;
        }

        return true;
    }

    public static boolean isReachedGoal() {
        return RobotPlayer.rc.getLocation().equals(goal);
    }

    public static boolean isNearToGoal() {
        return RobotPlayer.rc.getLocation().isAdjacentTo(goal);
    }

    public static boolean haveGoal() {
        return goal != null;
    }
}
