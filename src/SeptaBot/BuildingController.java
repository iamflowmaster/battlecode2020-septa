package SeptaBot;

import battlecode.common.GameActionException;

public abstract class BuildingController extends Controller {
    BuildingController() throws GameActionException {
        super();
        // отправляем информацию о строении
        TransactionUtils.addTransactionToTheQueue(TransactionUtils.buildingInfoMessage(
                RobotPlayer.rc.senseRobotAtLocation(RobotPlayer.rc.getLocation())), 3);
        TransactionUtils.tryToSendFromQueue();
    }
}
