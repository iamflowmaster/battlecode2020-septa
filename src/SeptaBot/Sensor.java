package SeptaBot;

import battlecode.common.*;

import java.util.Set;

public class Sensor {
    private static int terrainRefreshCooldownEndRound = 0;

    /**
     * Обновление локальной информации о среде
     */
    public static void refreshTerrain(boolean updateSoup) throws GameActionException {
        // проверяем можно ли выполнить обновление ландшафта
        int round = RobotPlayer.rc.getRoundNum();
        if (terrainRefreshCooldownEndRound > round)
            return;

        terrainRefreshCooldownEndRound = round + Constants.TERRAIN_REFRESH_COOLDOWN;

        System.out.println("REFRESH ROUND " + RobotPlayer.rc.getRoundNum());
        System.out.println("Sensor squared " + RobotPlayer.rc.getCurrentSensorRadiusSquared());
        // получаем локацию робота
        MapLocation robot_loc = RobotPlayer.rc.getLocation();

        // получаем целое число радиуса
        int squaredRadius = RobotPlayer.rc.getCurrentSensorRadiusSquared();
        int radius = (int) (Math.ceil(Math.sqrt(squaredRadius)) - 2);
        System.out.println("Radius " + radius);
        // находим половину длины строны вписанного в окружность квадрата
        // int squareSideLength = (int)(radius * Math.cos(Math.PI/4)/);

        // считываем каждый тайл, находящийся внутри видимой окружности
        for (int tile_y = robot_loc.y - radius; tile_y <= robot_loc.y + radius; tile_y++) {
            //System.out.println("TILE Y " + tile_y);
            int x_offset = (int) Math.ceil(
                    radius * Math.cos(Math.asin((double) Math.abs(robot_loc.y - tile_y) / radius)));

            //System.out.println("X OFFSET " + x_offset);

            for (int tile_x = robot_loc.x - x_offset; tile_x <= robot_loc.x + x_offset; tile_x++) {
                MapLocation tileLoc = new MapLocation(tile_x, tile_y);
                //System.out.println("TILE LOC " + tile_loc.toString());
                //System.out.println("CAN SENSE " + RobotPlayer.rc.canSenseLocation(tile_loc));

                if (Terrain.onTheMap(tileLoc)) {
                    // обновляем информацию
                    Terrain.setElevation(tileLoc, RobotPlayer.rc.senseElevation(tileLoc));
                    Terrain.setFlooding(tileLoc, RobotPlayer.rc.senseFlooding(tileLoc));
                    Terrain.setPollution(tileLoc, RobotPlayer.rc.sensePollution(tileLoc));

                    // обновляем суп в кэше объектов
                    if (updateSoup) {
                        // если есть на карте, то добавляем, иначе - удаляем
                        if (RobotPlayer.rc.senseSoup(tileLoc) > 0) {
                            // о супе еще никто не знает
                            if (!ObjectsCache.soup.contains(tileLoc)) {
                                // координата супа не отправляется, если рядом уже есть изведанный суп
                                soupTransaction: {
                                    for (MapLocation soupLoc : ObjectsCache.soup)
                                        if (tileLoc.isWithinDistanceSquared(soupLoc, squaredRadius))
                                            break soupTransaction;

                                    TransactionUtils.addTransactionToTheQueue(
                                            TransactionUtils.soupLocationMessage(tileLoc), 5);
                                }
                            }
                            ObjectsCache.soup.add(tileLoc);
                        } else {
                            ObjectsCache.soup.remove(tileLoc);  // если такого нет, то ошибки не будет
                        }
                    }
                }
            }
        }

        System.out.println("REFRESH END ROUND " + RobotPlayer.rc.getRoundNum());
    }

    public static void refreshRobots() {
        ObjectsCache.units.clear();
        Set<RobotInfo> removeBuildings = ObjectsCache.getBuildingsWithinDistanceSquared(
                RobotPlayer.rc.getLocation(), RobotPlayer.rc.getCurrentSensorRadiusSquared());

        for (RobotInfo robotInfo : RobotPlayer.rc.senseNearbyRobots()) {
            if (robotInfo.getType().isBuilding()) {
                if (ObjectsCache.buildings.contains(robotInfo)) {
                    removeBuildings.remove(robotInfo);
                } else {
                    TransactionUtils.addTransactionToTheQueue(TransactionUtils.buildingInfoMessage(robotInfo), 3);
                    ObjectsCache.buildings.add(robotInfo);
                }
            } else {
                ObjectsCache.units.add(robotInfo);
            }
        }

        // TODO сообщать всем о том, что зданий на этих локациях больше нет
        ObjectsCache.buildings.removeAll(removeBuildings);
    }

    public static void refreshAllyRobots() {
        ObjectsCache.units.clear();
        RobotInfo[] robots = RobotPlayer.rc.senseNearbyRobots(
                RobotPlayer.rc.getCurrentSensorRadiusSquared(), RobotPlayer.team);

        Set<RobotInfo> removeBuildings = ObjectsCache.getBuildingsWithinDistanceSquared(
                RobotPlayer.rc.getLocation(), RobotPlayer.rc.getCurrentSensorRadiusSquared(), RobotPlayer.team);

        for (RobotInfo robotInfo : robots) {
            if (robotInfo.type.isBuilding()) {
                if (ObjectsCache.buildings.contains(robotInfo)) {
                    removeBuildings.remove(robotInfo);
                } else {
                    TransactionUtils.addTransactionToTheQueue(TransactionUtils.buildingInfoMessage(robotInfo), 3);
                    ObjectsCache.buildings.add(robotInfo);
                }
            } else {
                ObjectsCache.units.add(robotInfo);
            }
        }

        // TODO сообщать всем о том, что зданий на этих локациях больше нет
        ObjectsCache.buildings.removeAll(removeBuildings);
    }

    public static void refreshEnemyRobots() throws GameActionException {
        ObjectsCache.units.clear();
        RobotInfo[] robots = RobotPlayer.rc.senseNearbyRobots(
                RobotPlayer.rc.getCurrentSensorRadiusSquared(), RobotPlayer.team.opponent());

        Set<RobotInfo> removeBuildings = ObjectsCache.getBuildingsWithinDistanceSquared(
                RobotPlayer.rc.getLocation(), RobotPlayer.rc.getCurrentSensorRadiusSquared(), RobotPlayer.team.opponent());

        for (RobotInfo robotInfo : robots) {
            if (robotInfo.type.isBuilding()) {
                // если строение было там до этого, то удалять не нужно
                if (ObjectsCache.buildings.contains(robotInfo)) {
                    removeBuildings.remove(robotInfo);
                } else {
                    TransactionUtils.addTransactionToTheQueue(TransactionUtils.buildingInfoMessage(robotInfo), 3);
                    ObjectsCache.buildings.add(robotInfo);
                }
            } else {
                ObjectsCache.units.add(robotInfo);
            }
        }

        // TODO сообщать всем о том, что зданий на этих локациях больше нет
        ObjectsCache.buildings.removeAll(removeBuildings);
    }

    public static void readTransactions() throws GameActionException {
        int curRound = RobotPlayer.rc.getRoundNum();
        for (int i = TransactionUtils.startReadFromRound; i < curRound; i++) {
            TransactionUtils.decodeTransactionBlock(RobotPlayer.rc.getBlock(i));
        }

        TransactionUtils.startReadFromRound = curRound;
    }
}
