package SeptaBot;
import battlecode.common.*;


public class HQController extends BuildingController {
    public int minerCreated = 0;

    public HQController() throws GameActionException {
        super();
    }

    public void run() throws GameActionException {
        Sensor.readTransactions();

        CombatUtils.tryToShootNearestDrone();

        // на ранней стадии создаем определенное количество шахтеров
        switch (globalTask) {
            case EARLY_MINERS:
                if (minerCreated < Constants.EARLY_MINERS_NUMBER) {
                    for (Direction dir : Direction.allDirections()) {
                        if (BuildUtils.tryBuild(RobotType.MINER, dir)) {
                            minerCreated++;

                            // самый первый майнер будет исследовать
                            if (minerCreated == 1) {
                                RobotInfo minerInfo = RobotPlayer.rc.senseRobotAtLocation(
                                        RobotPlayer.rc.adjacentLocation(dir));

                                TransactionUtils.addTransactionToTheQueue(
                                        TransactionUtils.callExpeditorMessage(minerInfo.ID), 1);
                            } else
                                if (minerCreated == 2) {
                                RobotInfo minerInfo = RobotPlayer.rc.senseRobotAtLocation(
                                        RobotPlayer.rc.adjacentLocation(dir));

                                // второй майнер будет строить первичные здания
                                TransactionUtils.addTransactionToTheQueue(
                                        TransactionUtils.callBuilderMessage(minerInfo.ID), 1);
                            }
                        }
                    }
                } else {
                    TransactionUtils.addTransactionToTheQueue(
                            TransactionUtils.globalTaskMessage(GlobalTask.PRIMARY_BUILDING), 5);

                    globalTask = GlobalTask.PRIMARY_BUILDING;
                }
                break;
            case PRIMARY_BUILDING:
                // как только построены нужные здания, начинается этап заземления
                if (ObjectsCache.getBuildingCount(RobotType.DESIGN_SCHOOL, RobotPlayer.team) == 1 &&
                        ObjectsCache.getBuildingCount(RobotType.FULFILLMENT_CENTER, RobotPlayer.team) == 1) {
                    TransactionUtils.addTransactionToTheQueue(
                            TransactionUtils.globalTaskMessage(GlobalTask.GROUNDING), 5);
                    globalTask = GlobalTask.GROUNDING;
                    System.out.println(globalTask);
                }
                break;
        }

        TransactionUtils.tryToSendFromQueue();
    }
}
