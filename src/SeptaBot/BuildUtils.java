package SeptaBot;

import battlecode.common.*;

public class BuildUtils {
    public static boolean tryBuild(RobotType type, Direction dir) throws GameActionException {
        if (RobotPlayer.rc.isReady() && RobotPlayer.rc.canBuildRobot(type, dir)) {
            RobotPlayer.rc.buildRobot(type, dir);
            return true;
        }

        return false;
    }
}
