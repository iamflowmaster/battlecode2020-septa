package SeptaBot;

import battlecode.common.*;

/**
 * Класс, в котором хранится информация о размерах карты и только та информация, которая может повлиять на
 * оптимальность построения пути.
 * Экземпляр этого объекта создается единажды.
 * Этот объект изменяется путем вызовов методов класса Sensor.
 */

public class Terrain {
    public static int map_width = RobotPlayer.rc.getMapWidth();
    public static int map_height = RobotPlayer.rc.getMapHeight();

    // эти значения инициализируются после создания определенного робота.
    // У зданий - нет, потому им не нужна эта информация, а юнитов - да, чтобы строить оптимальные пути.

    public static int[][] elevation;
    public static boolean[][] flooding;
    public static int[][] pollution;

    public static void init() {
        Terrain.elevation = new int[Terrain.map_width][Terrain.map_height];
        Terrain.flooding = new boolean[Terrain.map_width][Terrain.map_height];
        Terrain.pollution = new int[Terrain.map_width][Terrain.map_height];

        for (int x = 0; x < map_width; x++) {
            for (int y = 0; y < map_height; y++) {
                Terrain.elevation[x][y] = Integer.MAX_VALUE;
            }
        }
    }

    public static int getElevation(MapLocation l) {
        return elevation[l.x][l.y];
    }

    public static boolean getFlooding(MapLocation l) {
        return flooding[l.x][l.y];
    }

    public static int getPollution(MapLocation l) {
        return pollution[l.x][l.y];
    }

    public static void setElevation(MapLocation l, int value) {
        elevation[l.x][l.y] = value;
    }

    public static void setFlooding(MapLocation l, boolean value) {
        flooding[l.x][l.y] = value;
    }

    public static void setPollution(MapLocation l, int value) {
        pollution[l.x][l.y] = value;
    }

    public static boolean canStepFromTo(MapLocation from, MapLocation to) {
        System.out.println(from);
        System.out.println(to);
        // можно шагнуть, если еще не разведано или если разница в ландшафте позволяет и не затоплено
        return Terrain.onTheMap(from) && Terrain.onTheMap(to) && (Terrain.getElevation(to) == Integer.MAX_VALUE ||
                (Math.abs(Terrain.getElevation(from) - Terrain.getElevation(to)) <= GameConstants.MAX_DIRT_DIFFERENCE
                        && !Terrain.getFlooding(to)
                        && !ObjectsCache.containsBuildingAtLocation(to)
                        && !ObjectsCache.containsUnitAtLocation(to)
                ));
    }

    public static boolean onTheMap(MapLocation loc) {
        return loc.x > -1 && loc.x < Terrain.map_width && loc.y > -1 && loc.y < Terrain.map_height;
    }

    public static boolean isFloodedFully(MapLocation loc) {
        for (Direction dir : Direction.allDirections()) {
            MapLocation adjLoc = loc.translate(dir.dx, dir.dy);
            if (Terrain.onTheMap(adjLoc) && !Terrain.getFlooding(adjLoc))
                return false;
        }

        return true;
    }
}
