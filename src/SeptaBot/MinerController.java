package SeptaBot;

import battlecode.common.*;

import java.util.*;

public class MinerController extends UnitController {
    public MinerState state = MinerState.MINING;
    public boolean isPrimaryBuilder = false;
    public boolean isExpeditor = false;
    private RobotType buildingType;

    private MapLocation[] explorationPoints;
    private int curExplorationPoint;

    private boolean isGoingToRefinery = false;


    public MinerController() throws GameActionException {
        super();
    }

    public void run() throws GameActionException {
        super.run();

        // Обязательно проверяем возможность совершения действия, т.к
        // в дальнейшем коде ожидается, что действие возможно совершить.
        if (!RobotPlayer.rc.isReady())
            return;

        // исследование карты
        if (state == MinerState.EXPLORING) {
            // TODO исследование должно делиться на 2 группы: поиск HQ и случайное исследование
            if (isExpeditor && RobotPlayer.rc.getRoundNum() < Constants.EXPLORING_THRESHOLD &&
                    ObjectsCache.getFirstBuilding(RobotType.HQ, RobotPlayer.team.opponent()) == null) {
                updateExplorationGoal();
                BugMovement.moveToGoal();
            } else if (!tryToStartMining()) {
                if (BugMovement.haveGoal())
                    BugMovement.moveToGoal();
                else
                    BugMovement.setGoal(MoveUtils.getRandomMovementLocation());
            }
        }

        if (globalTask == GlobalTask.PRIMARY_BUILDING && isPrimaryBuilder) {
            // если хватает денег и еще не построили, то идем к месту постройки
            RobotType type = null;
            if (ObjectsCache.getBuildingCount(RobotType.DESIGN_SCHOOL, RobotPlayer.team) == 0 &&
                    RobotPlayer.rc.getTeamSoup() >= RobotType.DESIGN_SCHOOL.cost) {
                type = RobotType.DESIGN_SCHOOL;
            } else if (ObjectsCache.getBuildingCount(RobotType.FULFILLMENT_CENTER, RobotPlayer.team) == 0 &&
                    RobotPlayer.rc.getTeamSoup() >= RobotType.FULFILLMENT_CENTER.cost) {
                type = RobotType.FULFILLMENT_CENTER;
            }

            if (type != null) {
                // выбираем место постройки
                MapLocation buildingLoc = null;
                RobotInfo hqInfo = ObjectsCache.getFirstBuilding(RobotType.HQ, RobotPlayer.team);
                for (Direction dir : Direction.cardinalDirections()) {
                    MapLocation curLoc = hqInfo.location.translate(dir.dx * 2, dir.dy * 2);
                    if (Terrain.canStepFromTo(hqInfo.location.translate(dir.dx, dir.dy), curLoc)) {
                        buildingLoc = curLoc;
                        break;
                    }
                }

                startBuild(buildingLoc, type);
            }
        }

        if (state == MinerState.BUILDING) {
            if (BugMovement.isNearToGoal()) {
                System.out.println("Near to build loc");
                MapLocation robotLoc = RobotPlayer.rc.getLocation();
                if (BuildUtils.tryBuild(buildingType, robotLoc.directionTo(BugMovement.goal))) {
                    System.out.println("Builded a " + buildingType);
                    state = MinerState.MINING;
                }
            } else {
                BugMovement.moveToGoal();
            }
        }


        if (state == MinerState.MINING) {
            System.out.println("I am mining");
            mining();
        }

        // в конце отсылаем накопленные транзакции
        TransactionUtils.tryToSendFromQueue();
    }

    public void initExpedition() {
        MapLocation robotLoc = RobotPlayer.rc.getLocation();

        // находим отступы нашего HQ от краев карты
        MapLocation hqLoc = ObjectsCache.getFirstBuilding(RobotType.HQ, RobotPlayer.team).location;
        int xOffset = Math.min(Terrain.map_width - hqLoc.x, hqLoc.x);
        int yOffset = Math.min(Terrain.map_width - hqLoc.y, hqLoc.y);

        // ищем вражеский HQ на краях карты симметрично нашему
        explorationPoints = new MapLocation[] {
                new MapLocation(Terrain.map_width - xOffset, Terrain.map_height - yOffset),
                new MapLocation(xOffset, yOffset),
                new MapLocation(xOffset, Terrain.map_height - yOffset),
                new MapLocation(Terrain.map_width - xOffset, yOffset)
        };

        // делаем так, чтобы поиск проходит по квадрату
        Arrays.sort(explorationPoints, Comparator.comparingInt((x) -> -x.distanceSquaredTo(robotLoc)));

        MapLocation temp = explorationPoints[0];
        explorationPoints[0] = explorationPoints[1];
        explorationPoints[1] = temp;

        curExplorationPoint = 0;
        state = MinerState.EXPLORING;
        isExpeditor = true;
    }

    private void updateExplorationGoal() {
        if (BugMovement.isReachedGoal()
                || curExplorationPoint < RobotPlayer.rc.getRoundNum()/Constants.EXPLORING_THRESHOLD)
            curExplorationPoint++;

        BugMovement.setGoal(explorationPoints[curExplorationPoint]);
    }

    private void startBuild(MapLocation buildingLoc, RobotType buildingType) {
        BugMovement.setGoal(buildingLoc);
        state = MinerState.BUILDING;
        this.buildingType = buildingType;
    }

    private boolean tryToStartMining() {
        System.out.println("Trying to start mining.");
        // TODO сделать так, чтобы суп выбирался от брижайшего перерабатывающего здания
        MapLocation nearestSoup = ObjectsCache.getNearestSoup(true);
        if (nearestSoup == null) {
            state = MinerState.EXPLORING;
            return false;
        }

        BugMovement.setGoal(nearestSoup);
        System.out.println("Nearest soup is " + nearestSoup);
        System.out.println(ObjectsCache.soup);

        state = MinerState.MINING;
        return true;
    }

    private boolean mining() throws  GameActionException {
        MapLocation robotLoc = RobotPlayer.rc.getLocation();

        // если шахтер рядом с целью, то либо он стоит около супа, либо около производящего здания
        if (!BugMovement.haveGoal()) {
            System.out.println("Trying to restart mining because lost the goal");
            tryToStartMining();
        } else if (BugMovement.isNearToGoal()) {
            System.out.println("Near to the goal");
            if (isGoingToRefinery) {
                System.out.println("I am going to refinery");

                Direction refineryDir = robotLoc.directionTo(BugMovement.goal);
                if (RobotPlayer.rc.canDepositSoup(refineryDir)) {
                    RobotPlayer.rc.depositSoup(refineryDir, RobotPlayer.rc.getSoupCarrying());
                }

                tryToStartMining();
                isGoingToRefinery = false;
            } else {
                System.out.println("I have " + RobotPlayer.rc.getSoupCarrying() + " soup");
                System.out.println("Soup location is " + BugMovement.goal);
                // собираем суп
                Direction soupDir = robotLoc.directionTo(BugMovement.goal);
                // идем к зданию, способному обработать суп, если полный инвентарь
                if (RobotPlayer.rc.getSoupCarrying() >= Constants.MINER_INVENTORY_MAXIMUM) {
                    // если нет поблизости обработки, то строим ей
//                    if (!ObjectsCache.isLocationWithinAnyRefineryRadius(robotLoc)) {
//                        System.out.println("Need to build a refinery");
//                        for(Direction dir : RobotPlayer.directions) {
//                            MapLocation buildLoc = robotLoc.translate(dir.dx, dir.dy);
//                            if (Terrain.canStepFromTo(robotLoc, buildLoc)) {
//                                startBuild(buildLoc, RobotType.REFINERY);
//                                return false;
//                            }
//                        }
//                        System.out.println("Can't build refinery, going to explore");
//
//                        state = MinerState.EXPLORING;
//                        return false;
//                    }


                    RobotInfo nearestRefinery = ObjectsCache.getNearestRefinery();
                    if (nearestRefinery == null) {
                        // TODO написать обработку варианта, когда невозможно найти обрабатывающее здание
                        System.out.println("No refinery");
                        return false;
                    }

                    BugMovement.setGoal(nearestRefinery.location);
                    isGoingToRefinery = true;
                    System.out.println("GO TO REFINERY");
                } else if (RobotPlayer.rc.canMineSoup(soupDir)) {
                    RobotPlayer.rc.mineSoup(soupDir);
                } else {
                    tryToStartMining();
                }
            }
        } else {
            System.out.println("Going to goal " + BugMovement.goal);
            BugMovement.moveToGoal();
        }

        return true;
    }
}


enum MinerState {
    EXPLORING,
    MINING,
    BUILDING
}