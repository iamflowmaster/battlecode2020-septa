package SeptaBot;

import battlecode.common.GameActionException;

public abstract class Controller {
    public GlobalTask globalTask = GlobalTask.EARLY_MINERS;

    Controller() throws GameActionException {

    }

    public abstract void run() throws GameActionException;

    public void postInit() throws GameActionException {
        Sensor.readTransactions();
    }
}
