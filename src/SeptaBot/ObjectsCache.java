package SeptaBot;

import battlecode.common.*;
import java.util.*;

public class ObjectsCache {
    public static Set<MapLocation> soup = new HashSet<>();

    public static Set<RobotInfo> buildings = new HashSet<>();
    public static Set<RobotInfo> units = new HashSet<>();

    public static MapLocation getNearestSoupWithinDistanceSquared(int distanceSquared) {
        MapLocation robotLoc = RobotPlayer.rc.getLocation();
        MapLocation nearestSoup = getNearestSoup(true);
        return (nearestSoup == null || robotLoc.distanceSquaredTo(nearestSoup) < distanceSquared) ? null : nearestSoup;
    }

    public static MapLocation getNearestSoup(boolean free) {
        MapLocation robotLoc = RobotPlayer.rc.getLocation();

        MapLocation nearestSoup = null;
        int minDistance = Integer.MAX_VALUE;

        for (MapLocation soupLoc : soup) {
            int curDistance = robotLoc.distanceSquaredTo(soupLoc);
            if (curDistance < minDistance) {
                // если нам нужен свободный, то выполняем эту проверку
                if (free && Terrain.isFloodedFully(soupLoc))
                    continue;

                nearestSoup = soupLoc;
                minDistance = curDistance;
            }
        }

        return nearestSoup;
    }

    public static RobotInfo getNearestUnit(Team team, RobotType unitType) {
        MapLocation robotLoc = RobotPlayer.rc.getLocation();

        RobotInfo nearestUnit = null;
        int minDistance = Integer.MAX_VALUE;

        for (RobotInfo robotInfo : ObjectsCache.units) {
            int curDistance = robotLoc.distanceSquaredTo(robotInfo.location);
            if ((robotInfo.team == team) && (curDistance < minDistance) && robotInfo.type == unitType) {
                nearestUnit = robotInfo;
                minDistance = curDistance;
            }
        }

        return nearestUnit;
    }

    public static RobotInfo getNearestBuilding(Team team, RobotType buildingType) {
        MapLocation robotLoc = RobotPlayer.rc.getLocation();

        RobotInfo nearestBuilding = null;
        int minDistance = Integer.MAX_VALUE;

        for (RobotInfo robotInfo : ObjectsCache.buildings) {
            int curDistance = robotLoc.distanceSquaredTo(robotInfo.location);
            if ((robotInfo.team == team) && (curDistance < minDistance) && robotInfo.type == buildingType) {
                nearestBuilding = robotInfo;
                minDistance = curDistance;
            }
        }

        return nearestBuilding;
    }


    public static Set<RobotInfo> getBuildingsWithinDistanceSquared(MapLocation loc, int squareDistance) {
        Set<RobotInfo> result = new HashSet<>();
        for (RobotInfo robotInfo : buildings) {
            if (robotInfo.location.isWithinDistanceSquared(loc, squareDistance))
                result.add(robotInfo);
        }

        return result;
    }

    public static Set<RobotInfo> getBuildingsWithinDistanceSquared(MapLocation loc, int squareDistance, Team team) {
        Set<RobotInfo> result = new HashSet<>();

        for (RobotInfo robotInfo : buildings) {
            if (robotInfo.location.isWithinDistanceSquared(loc, squareDistance)  && robotInfo.team == team)
                result.add(robotInfo);
        }

        return result;
    }

    public static boolean containsUnitAtLocation(MapLocation loc) {
        for(RobotInfo robotInfo : ObjectsCache.units)
            if (robotInfo.location.equals(loc))
                return true;

        return false;
    }

    public static boolean containsBuildingAtLocation(MapLocation loc) {
        for(RobotInfo robotInfo : ObjectsCache.buildings)
            if (robotInfo.location.equals(loc))
                return true;

        return false;
    }

    public static int getBuildingCount(RobotType type) {
        int count = 0;

        for (RobotInfo robotInfo : buildings)
            if (robotInfo.type == type)
                count++;

        return count;
    }

    public static int getBuildingCount(RobotType type, Team team) {
        int count = 0;

        for (RobotInfo robotInfo : buildings)
            if (robotInfo.type == type && robotInfo.team == team)
                count++;

        return count;
    }

    public static RobotInfo getFirstBuilding(RobotType type) {
        for (RobotInfo robotInfo : buildings) {
            if (robotInfo.type == type)
                return robotInfo;
        }

        return null;
    }

    public static RobotInfo getFirstBuilding(RobotType type, Team team) {
        for (RobotInfo robotInfo : buildings) {
            if (robotInfo.type == type && robotInfo.team == team)
                return robotInfo;
        }

        return null;
    }

    public static RobotInfo getNearestRefinery() {
        for (RobotInfo robotInfo : buildings) {
            // HQ считается за рафинарнь, если мы еще не построили вокруг здания
            if ((robotInfo.type == RobotType.HQ || robotInfo.type == RobotType.REFINERY) &&
                    robotInfo.team == RobotPlayer.team)
                return robotInfo;
        }

        return null;
    }

    public static boolean isLocationWithinAnyRefineryRadius(MapLocation loc) {
        for (RobotInfo robotInfo : buildings) {
            if ((robotInfo.type == RobotType.HQ || robotInfo.type == RobotType.REFINERY) &&
                    robotInfo.team == RobotPlayer.team &&
                    loc.isWithinDistanceSquared(robotInfo.location, Constants.REFINERY_MIN_DISTANCE)
            ) {
                return true;
            }
        }

        return false;
    }
}
