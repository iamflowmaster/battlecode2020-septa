package SeptaBot;

import battlecode.common.*;

public class NetGunController extends BuildingController {
    NetGunController() throws GameActionException {
        super();
    }

    public void run() throws GameActionException {
        Sensor.refreshEnemyRobots();
        CombatUtils.tryToShootNearestDrone();
    }
}
