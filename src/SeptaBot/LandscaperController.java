package SeptaBot;

import battlecode.common.*;

public class LandscaperController extends UnitController {
    public boolean isGrounder = false;
    public MapLocation groundingLoc;

    LandscaperController() throws GameActionException {
        super();
    }

    public void run() throws GameActionException {
        super.run();

        // Обязательно проверяем возможность совершения действия, т.к
        // в дальнейшем коде ожидается, что действие возможно совершить.
        if (!RobotPlayer.rc.isReady())
            return;

        if (isGrounder) {
            System.out.println("I am grounder");
            if (BugMovement.isReachedGoal()) {
                if (RobotPlayer.rc.getDirtCarrying() > 0) {
                    RobotPlayer.rc.depositDirt(Direction.CENTER);
                } else {
                    findDirtForGrounding();
                }
            } else {
                BugMovement.moveToGoal();
            }
        }

    }

    public void initGrounding(MapLocation groundingLoc) {
        BugMovement.setGoal(groundingLoc);
        isGrounder = true;
    }

    private void findDirtForGrounding() throws GameActionException {
        MapLocation hqLoc = ObjectsCache.getFirstBuilding(RobotType.HQ, RobotPlayer.team).location;
        Direction oppositeHQDir = RobotPlayer.rc.getLocation().directionTo(hqLoc).opposite();

        System.out.println(oppositeHQDir);
        Direction checkDir = oppositeHQDir;

        if (RobotPlayer.rc.canDigDirt(checkDir) &&
                !ObjectsCache.containsUnitAtLocation(RobotPlayer.rc.adjacentLocation(checkDir))) {
            RobotPlayer.rc.digDirt(checkDir);
            return;
        }

        checkDir = oppositeHQDir.rotateLeft();
        if (RobotPlayer.rc.canDigDirt(checkDir) &&
                !ObjectsCache.containsUnitAtLocation(RobotPlayer.rc.adjacentLocation(checkDir))) {
            RobotPlayer.rc.digDirt(checkDir);
            return;
        }

        checkDir = oppositeHQDir.rotateRight();
        if (RobotPlayer.rc.canDigDirt(checkDir) &&
                !ObjectsCache.containsUnitAtLocation(RobotPlayer.rc.adjacentLocation(checkDir))){
            RobotPlayer.rc.digDirt(checkDir);
            return;
        }
    }
}
