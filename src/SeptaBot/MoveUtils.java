package SeptaBot;

import battlecode.common.*;
import java.util.*;

public class MoveUtils {
    public static boolean tryToMove(Direction dir) throws GameActionException {
        if (dir == null)
            return false;

        MapLocation robotLoc = RobotPlayer.rc.getLocation();
        if (!RobotPlayer.rc.canMove(dir) || Terrain.getFlooding(robotLoc.translate(dir.dx, dir.dy)))
            return false;

        RobotPlayer.rc.move(dir);
        return true;
    }

    public static int normalizeDirectionOrdinal(int ordinal, int dirNumber) {
        if (ordinal >= dirNumber)
            ordinal -= dirNumber;
        else if (ordinal < 0)
            ordinal += dirNumber;

        return ordinal;
    }

    public static boolean noObstacleBetweenLocations(MapLocation src, MapLocation dest, int lengthCheck) {
        Direction dir;
        MapLocation nextSrc;

        int curCheckLength = 0;

        while (!src.equals(dest) && curCheckLength < lengthCheck) {
            dir = src.directionTo(dest);
            nextSrc = src.translate(dir.dx, dir.dy);
            if (!Terrain.canStepFromTo(src, nextSrc))
                return false;

            src = nextSrc;
            curCheckLength++;
        }

        return true;
    }

    public static MapLocation getRandomMovementLocation() {
        MapLocation robotLoc = RobotPlayer.rc.getLocation();
        MapLocation randomLoc;
        do {
            randomLoc = new MapLocation(
                    (int) Math.round(robotLoc.x + 1 + Math.random() * 10),
                    (int) Math.round(robotLoc.y + 1 + Math.random() * 10));
        } while (!Terrain.onTheMap(randomLoc));

        return randomLoc;
    }
}
